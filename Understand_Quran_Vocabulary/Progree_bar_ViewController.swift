//
//  Progree_bar_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/7/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class Progree_bar_ViewController: UIViewController, UISearchBarDelegate, UISearchControllerDelegate {

    var search_array = Lessons_ViewController.items
    
    @IBOutlet weak var yes_btn: UIButton!
    
    @IBOutlet weak var progress_Value_lbl: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var search_bar_tableView: UITableView!
    
    var searchController = UISearchController(searchResultsController: nil)
    
    var isGreen = true
    var isChecked = false
    var current: Int = 0
    let maxValue = 10
    var progressBarValue : Float = 0
    
        override func viewDidLoad() {
            super.viewDidLoad()
         //   btnStart()
            // Do any additional setup after loading the view, typically from a nib.
            search_bar_tableView.delegate = self
            search_bar_tableView.dataSource = self
            
            progressView.progress = 0.0
            progressView.transform = progressView.transform.scaledBy(x: 1, y: 10)
            progressView.layer.cornerRadius = 10
            progressView.clipsToBounds = true
            progressView.layer.sublayers![1].cornerRadius = 10
            progressView.subviews[1].clipsToBounds = true
   
            dataModelset()
            searchBarSetup()
    
    }
    @IBAction func yes_btn_action(_ sender: Any) {
         current+=1
         progressBar()
      
    }
    
    @IBAction func no_btn_action(_ sender: Any) {
   
         current-=1
        progressBar()
  
    }
    func progressBar() {
        let i = current
        var saveProgressValue: Float
        
        if i <= maxValue {
            let ratio = Float(i) / Float(maxValue)
            
            progressView.progress = Float(ratio)
            saveProgressValue = progressView.progress
            progressBarValue = saveProgressValue
            print(progressBarValue)
            progress_Value_lbl.text = "progress is: \(Float(ratio))%"
            isChecked = true
            
        }
    }
    func dataModelset(){
        search_array = [
        
            Lesson(s_no: 1 , lesson_no: "Part - 1a" , word_type : "Verb" ,word_name : "اَعُوْذُ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "I seek refuge",word_urdu_meaning: "", root_word: "ع و ذ", verb_code: "(قا)", word_count: "7", similar_word: "" , isSelect: true , progress_value: 0),
            
            Lesson(s_no: 2, lesson_no: "Part - 1a" , word_type : "Noun" ,word_name : "اللّه", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "Allah",word_urdu_meaning: "اللّه", root_word: "", verb_code: "" , word_count: "2550", similar_word: "", isSelect: true , progress_value: 0),
            
            Lesson(s_no: 3, lesson_no: "Bart - 2a" , word_type : "Letter" ,word_name : "مِنْ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "from", word_urdu_meaning: "سے", root_word: "", verb_code: "" , word_count: "2471", similar_word: "" , isSelect: false , progress_value: 0),
            
            Lesson(s_no: 4, lesson_no: "Bart - 2a" , word_type : "Noun" , word_name : "شَيْطَان" , word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "satan", word_urdu_meaning: "شیطان", root_word: "ش ط ن", verb_code: "" , word_count: "88", similar_word: "شَيَاطِيْن", isSelect: true , progress_value: 0),
            
            Lesson(s_no: 5, lesson_no: "Bart - 2a" , word_type : "Noun" , word_name : "رَجِيْم", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "the outcast", word_urdu_meaning: "مردود، دھتکارا ہوا", root_word: "ر ج م", verb_code: "(نـ)" , word_count: "6", similar_word: "", isSelect: false , progress_value: 0)
        ]
    }
    private func searchBarSetup(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        
    }
    
    @IBAction func search_btn(_ sender: UIBarButtonItem) {
        
           searchController.hidesNavigationBarDuringPresentation = false

           self.searchController.searchBar.delegate = self
           present(searchController, animated: true, completion: nil)
        
           searchController.searchResultsUpdater = self
           dataModelset()
        
    }
}
extension Progree_bar_ViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        
        if searchText == ""{
            dataModelset()
           print("this is the value when searchBar has empty value")
        }else{
            search_array = search_array.filter{
                    $0.lesson_no.contains(searchText)
            }
        }
        search_bar_tableView.reloadData()
        
    }
}
extension Progree_bar_ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search_bar_TableViewCell") as? search_bar_TableViewCell
        cell?.searchLbl.text = search_array[indexPath.row].lesson_no
         return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("this is the did select of tableView\(indexPath)")
    }
}


  
