//
//  Completed_TableViewCell.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/5/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class Completed_TableViewCell: UITableViewCell {

   
    @IBOutlet weak var lesson_no_lbl: UILabel!
    @IBOutlet weak var word_type: UILabel!
    @IBOutlet weak var word_name_lbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
