//
//  lesson.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/7/19.
//  Copyright © 2019 MacBook. All rights reserved.
//
import UIKit
import Foundation

struct Lesson :Codable{
    var s_no : Int
    var lesson_no : String
    var word_type : String
    var word_name : String
    var word_example: String
    var eng_translation: String
    var urdu_translation: String
    var word_eng_meaning: String
    var word_urdu_meaning: String
    var root_word: String
    var verb_code: String
    var word_count: String
    var similar_word: String
    var isSelect: Bool?
    var progress_value: Int
    
    enum CodingKeys: String, CodingKey {
        
        case s_no = "s_no"
        case lesson_no = "lesson_no"
        case word_type = "word_type"
        case word_name = "word_name"
        case word_example = "word_example"
        case eng_translation = "eng_translation"
        case urdu_translation = "urdu_translation"
        case word_eng_meaning = "word_eng_meaning"
        case word_urdu_meaning = "word_urdu_meaning"
        case root_word = "root_word"
        case verb_code = "verb_code"
        case word_count = "word_count"
        case similar_word = "similar_word"
        case isSelect = "isSelect"
        case progress_value = "progress_value"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        s_no = try values.decodeIfPresent(Int.self, forKey: .s_no) ?? -1
        lesson_no = try values.decodeIfPresent(String.self, forKey: .lesson_no) ?? ""
        word_name = try values.decodeIfPresent(String.self, forKey: .word_name) ?? ""
        word_type = try values.decodeIfPresent(String.self, forKey: .word_type) ?? ""
        word_example = try values.decodeIfPresent(String.self, forKey: .word_example) ?? ""
        eng_translation = try values.decodeIfPresent(String.self, forKey: .eng_translation) ?? ""
        urdu_translation = try values.decodeIfPresent(String.self, forKey: .urdu_translation) ?? ""
        word_eng_meaning = try values.decodeIfPresent(String.self, forKey: .word_eng_meaning) ?? ""
        word_urdu_meaning = try values.decodeIfPresent(String.self, forKey: .word_urdu_meaning) ?? ""
        root_word = try values.decodeIfPresent(String.self, forKey: .root_word) ?? ""
        verb_code = try values.decodeIfPresent(String.self, forKey: .verb_code) ?? ""
        word_count = try values.decodeIfPresent(String.self, forKey: .word_count) ?? ""
        similar_word = try values.decodeIfPresent(String.self, forKey: .similar_word) ?? ""
        isSelect = try values.decodeIfPresent(Bool.self, forKey: .isSelect) ?? false
        progress_value = try values.decodeIfPresent(Int.self, forKey: .progress_value) ?? -1
        
    }
    
    init(s_no: Int, lesson_no: String, word_type : String , word_name:String, word_example:String, eng_translation:String, urdu_translation:String, word_eng_meaning:String ,word_urdu_meaning: String, root_word:String, verb_code:String , word_count: String, similar_word: String, isSelect: Bool?, progress_value: Int) {
        self.s_no = s_no
        self.lesson_no = lesson_no
        self.word_type = word_type
        self.word_name =  word_name
        self.word_example = word_example
        self.eng_translation = eng_translation
        self.urdu_translation = urdu_translation
        self.word_eng_meaning = word_eng_meaning
        self.word_urdu_meaning = word_urdu_meaning
        self.root_word = root_word
        self.verb_code = verb_code
        self.word_count = word_count
        self.similar_word = similar_word
        self.isSelect = isSelect
        self.progress_value = progress_value
    }
}

