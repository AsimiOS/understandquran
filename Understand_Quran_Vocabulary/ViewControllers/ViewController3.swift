//
//  ViewController3.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/3/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var yes_image: UIImageView!
    
    @IBOutlet weak var no_image: UIImageView!
    
    var isCheck: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isCheck = false
        // Do any additional setup after loading the view.
    }
    @IBAction func no_btn_action(_ sender: Any) {
        isCheck = true
        no_image.image = UIImage(named: "Asset 44")
        yes_image.image = UIImage(named: "Asset 37")
        
    }
    
    @IBAction func yes_btn_action(_ sender: Any) {
        isCheck = true
        yes_image.image = UIImage(named: "Asset 43")
        no_image.image = UIImage(named: "Asset 35")
        
        
        
    }
    @IBAction func next_btn(_ sender: Any) {
        if isCheck == false {
            let alert = UIAlertController.init(title: "Attention!", message: "Do you Understand word, Please select one option(Y/N)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController3") as? ViewController3
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func previous_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    

}
