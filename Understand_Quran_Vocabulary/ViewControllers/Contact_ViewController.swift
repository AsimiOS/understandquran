//
//  Contact_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class Contact_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func call_btn(_ sender: Any) {
        
        UIApplication.shared.openURL(NSURL(string: "tel://+447450263064")! as URL)
    }
    
    @IBAction func email_btn(_ sender: Any) {
        let supportEmail = "support@understandquran.com"
        if let emailURL = URL(string: "mailto:\(supportEmail)"), UIApplication.shared.canOpenURL(emailURL)
        {
            UIApplication.shared.open(emailURL, options: [:], completionHandler: nil)
        }
    
    }
    
    @IBAction func website_btn(_ sender: Any) {
        if let url = URL(string: "http://www.understandquran.com/"){
            UIApplication.shared.open(url)
        }
        
      
    }
    @IBAction func facebook_btn(_ sender: Any) {
        if let url = URL(string: "https://www.facebook.com/understandquran/"){
            UIApplication.shared.open(url)
        }
        
    }
    
    @IBAction func youtube_btn(_ sender: Any) {
        if let url = URL(string: "https://www.youtube.com/user/understandquran/videos"){
            UIApplication.shared.open(url)
        }
    }
    
}
