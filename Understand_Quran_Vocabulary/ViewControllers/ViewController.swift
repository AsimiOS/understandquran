//
//  ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/2/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var s_no_lbl: UILabel!
    @IBOutlet weak var lesson_no_lbl: UILabel!
    @IBOutlet weak var noun_imageView: UIImageView!
    @IBOutlet weak var verb_imageView: UIImageView!
    @IBOutlet weak var letter_imageView: UIImageView!
    
    @IBOutlet weak var word_count: UILabel!
    @IBOutlet weak var word_name_lbl: UILabel!
    @IBOutlet weak var root_words_lbl: UILabel!
    @IBOutlet weak var Verb_code_lbl: UILabel!
    @IBOutlet weak var English_wordMeaning: UILabel!
    @IBOutlet weak var urdu_wordMeaning: UILabel!
    @IBOutlet weak var similar_words_lbl: UILabel!
    
    @IBOutlet weak var word_example: UILabel!
    @IBOutlet weak var english_Translation_lbl: UILabel!
    @IBOutlet weak var urdu_translation: UILabel!
    
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var yes_image: UIImageView!
    @IBOutlet weak var no_image: UIImageView!
    @IBOutlet weak var progress_bar_value: UILabel!
    
    @IBOutlet weak var next_btn: UIButton!
    @IBOutlet weak var previous_btn: UIButton!
    @IBOutlet weak var next_btn_lbl: UILabel!
    @IBOutlet weak var previous_btn_lbl: UILabel!
    
    
    var current: Int = 10
    let maxValue = 100
    var progressBarValue : Float = 0
    var saveProgressValue: Double?
    
    var isCheck: Bool?
    var isRadio_select : Bool?
    

    var selectedItem: Lesson?
    var indexPath_value: IndexPath?
    var selectedLesson : [Lesson]?
    
    var indexSelect : IndexPath?
    var checkCount : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        english_Translation_lbl.layer.borderWidth = 0.5
        
        english_Translation_lbl.layer.cornerRadius = 1
        english_Translation_lbl.layer.borderColor = UIColor.lightGray.cgColor
        
        urdu_translation.layer.borderWidth = 0.5
        urdu_translation.layer.cornerRadius = 1
        urdu_translation.layer.borderColor = UIColor.lightGray.cgColor
        
        checkCount = indexSelect?.row
        if checkCount == 0 {
            previous_btn.isHidden = true
            previous_btn_lbl.isHidden = true
        }
        isRadio_select = false
        isCheck = false
        let obj : Lesson = (selectedLesson![(indexSelect?.row)!] )
        
        print(obj.s_no)
        
        asignValue(object: obj)
        
        progressView.progress = 0.0
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 10)
        progressView.layer.cornerRadius = 5
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 5
        progressView.subviews[1].clipsToBounds = true
    
        // Search controller
       
    }
   

    func asignValue(object : Lesson ){
        s_no_lbl.text = String(object.s_no)
        lesson_no_lbl.text = object.lesson_no
        word_name_lbl.text = object.word_name
        root_words_lbl.text = object.root_word
        Verb_code_lbl.text = object.verb_code
        English_wordMeaning.text = object.word_eng_meaning
        urdu_wordMeaning.text = object.word_urdu_meaning
        similar_words_lbl.text = object.similar_word
        word_example.text = object.word_example
        english_Translation_lbl.text = object.eng_translation
        urdu_translation.text = object.urdu_translation
        word_count.text = object.word_count
       // progress_bar_value.text = "\(object.progress_value)"
        
        if object.word_type == "Noun"{
          verb_imageView.image = UIImage(named: "Asset 27")
          letter_imageView.image = UIImage(named: "Asset 28")
            
          noun_imageView.image = UIImage(named: "Asset 30")
          noun_imageView.backgroundColor =  UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
          word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
        }else if object.word_type == "Verb"{
            noun_imageView.image = UIImage(named: "Asset 29")
            letter_imageView.image = UIImage(named: "Asset 28")
            
            verb_imageView.image = UIImage(named: "Asset 30")
            verb_imageView.backgroundColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)

        }else{
            noun_imageView.image = UIImage(named: "Asset 29")
            verb_imageView.image = UIImage(named: "Asset 27")
            
            letter_imageView.image = UIImage(named: "Asset 30")
            letter_imageView.backgroundColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
            word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
            }
    }
    
   
    func incrementCount(number: inout Int) {
        number += 1
    }
    func decrementCount(number: inout Int) {
        number -= 1
    }
    @IBAction func next_btn(_ sender: Any) {
        print(checkCount!)
        if isRadio_select == false {
            let alert = UIAlertController.init(title: "Attention!", message: "Do you Understand word, Please select one option(Y/N)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
        if isCheck == true{
            var obj : Lesson = (selectedLesson![checkCount!])
                obj.isSelect = true
                obj.progress_value = Int(progressBarValue)
            if Constant.completed_word_array.contains(where: { $0.lesson_no == obj.lesson_no}){
                print("already exist")
            }else{
                Constant.completed_word_array.append(obj)
            }
            do{
                let lessonData = try! JSONEncoder().encode(Constant.completed_word_array)
                UserDefaults.standard.set(lessonData, forKey: "completed")
            }catch{
                print("error:\n\(error)\n")
            }
            
             //   completed_array1.append(obj)
        }else{
            var obj : Lesson = (selectedLesson![checkCount!])
              obj.isSelect = false
              obj.progress_value = Int(progressBarValue)
            if Constant.pending_word_array.contains(where: { $0.lesson_no == obj.lesson_no}){
                print("already exist")
            }else{
                Constant.pending_word_array.append(obj)
            }
            
            do {
                let lessonData = try! JSONEncoder().encode(Constant.pending_word_array)
                UserDefaults.standard.set(lessonData, forKey: "pending")
            } catch {
                print("error:\n\(error)\n")
            }
            
           
        }
        previous_btn.isHidden = false
        previous_btn_lbl.isHidden = false
        
        yes_image.image = UIImage(named: "Asset 37")
        
        no_image.image = UIImage(named: "Asset 35")
        isRadio_select = false
        isCheck = false
        
        incrementCount(number: &checkCount!)
        print(checkCount!)
        if checkCount == selectedLesson?.count{
            next_btn.isHidden = true
            next_btn_lbl.isHidden = true
        }else{
            let obj : Lesson = (selectedLesson![checkCount!])
            asignValue(object: obj)
        }
      }
    }
    @IBAction func previous_btn(_ sender: Any) {
     
        if isRadio_select == false {
            let alert = UIAlertController.init(title: "Attention!", message: "Do you Understand word, Please select one option(Y/N)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            if isCheck == true{
                var obj : Lesson = (selectedLesson![checkCount!])
                obj.isSelect = true
                obj.progress_value = Int(progressBarValue)
                if Constant.completed_word_array.contains(where: { $0.lesson_no == obj.lesson_no}){
                    print("already exist")
                }else{
                    Constant.completed_word_array.append(obj)
                }
            }else{
                var obj : Lesson = (selectedLesson![checkCount!])
                obj.isSelect = false
                obj.progress_value = Int(progressBarValue)
                if Constant.pending_word_array.contains(where: { $0.lesson_no == obj.lesson_no}){
                    print("already exist")
                }else{
                Constant.pending_word_array.append(obj)
                }
            }
            
        if checkCount! > 0 {
           decrementCount(number: &checkCount!)
            let obj : Lesson = (selectedLesson![checkCount!] )
            asignValue(object: obj)
            
            isCheck = false
            next_btn.isHidden = false
            next_btn_lbl.isHidden = false
            if checkCount == 0 {
                previous_btn.isHidden = true
                previous_btn_lbl.isHidden = true
            }
        }else{
            previous_btn.isHidden = true
            previous_btn_lbl.isHidden = true
        }
        
        yes_image.image = UIImage(named: "Asset 37")
        no_image.image = UIImage(named: "Asset 35")
      }
    
    }
    
    @IBAction func no_btn_action(_ sender: Any) {
       
        if isCheck == true {
        no_image.image = UIImage(named: "Asset 44")
        yes_image.image = UIImage(named: "Asset 37")
        
        current-=1
        progressBar()
        isCheck = false
        isRadio_select = true
        
        }else{
            no_image.image = UIImage(named: "Asset 44")
            yes_image.image = UIImage(named: "Asset 37")
            isRadio_select = true
        }
      
    }
    
    @IBAction func yes_btn_action(_ sender: Any) {
        if isCheck == false {
        yes_image.image = UIImage(named: "Asset 43")
        no_image.image = UIImage(named: "Asset 35")
            
        current+=1
        progressBar()
        isCheck = true
        isRadio_select = true
       
        }else{
             print("already checked")
        }
        
        
    }
   
    func progressBar() {
        let i = current
       
        if i <= maxValue {
            let ratio = Float(i) / Float(maxValue)
            
            progressView.progress = Float(ratio)
            saveProgressValue = Double(progressView.progress)
            progressBarValue = Float(saveProgressValue!)
            print(progressBarValue)
            progress_bar_value.text = "\(progressBarValue)%"
        
        }
    }
    
    func uniqueElementsFrom(array: [Lesson]) {
        for value in array {
            
            if Constant.completed_word_array.contains(where: {($0.s_no != value.s_no)}){
                Constant.completed_word_array.append(value)
            }
        }
    }
    
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}
