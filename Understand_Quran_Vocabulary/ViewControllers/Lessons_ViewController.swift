//
//  Lessons_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/3/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import SideMenu

class Lessons_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UISearchBarDelegate {
   
    @IBOutlet weak var lesson_name_tableview: UITableView?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var search_view: UIView!
    @IBOutlet weak var search_tf: UITextField!
    
    var searchController = UISearchController(searchResultsController: nil)
    
    var isChecked : Bool = true
    var searchBarArray = [Lesson]()
    
    private var settingItem: [Lesson] = []
    static var items: [Lesson] = [
        Lesson(s_no: 1 , lesson_no: "Part - 1a" , word_type : "Verb" ,word_name : "اَعُوْذُ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "I seek refuge",word_urdu_meaning: "", root_word: "ع و ذ", verb_code: "(قا)", word_count: "7", similar_word: "" , isSelect: true , progress_value: 0),
        
        Lesson(s_no: 2, lesson_no: "Part - 1a" , word_type : "Noun" ,word_name : "اللّه", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "Allah",word_urdu_meaning: "اللّه", root_word: "", verb_code: "" , word_count: "2550", similar_word: "", isSelect: true , progress_value: 0),
        
        Lesson(s_no: 3, lesson_no: "Part - 1a" , word_type : "Letter" ,word_name : "مِنْ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "from", word_urdu_meaning: "سے", root_word: "", verb_code: "" , word_count: "2471", similar_word: "" , isSelect: false , progress_value: 0),
        
        Lesson(s_no: 4, lesson_no: "Part - 1a" , word_type : "Noun" , word_name : "شَيْطَان" , word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "satan", word_urdu_meaning: "شیطان", root_word: "ش ط ن", verb_code: "" , word_count: "88", similar_word: "شَيَاطِيْن", isSelect: true , progress_value: 0),
       
        Lesson(s_no: 5, lesson_no: "Part - 1a" , word_type : "Noun" , word_name : "رَجِيْم", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "the outcast", word_urdu_meaning: "مردود، دھتکارا ہوا", root_word: "ر ج م", verb_code: "(نـ)" , word_count: "6", similar_word: "", isSelect: false , progress_value: 0),
       
        Lesson(s_no: 6, lesson_no: "Part - 1b" , word_type : "Noun" , word_name : "هُوَ", word_example: "قُلْ هُوَ اللهُ اَحَدٌ " , eng_translation: "Say, “He is Allah, (Who is) One", urdu_translation: "کہہ دیجیے! وہ اللہ ایک ہے", word_eng_meaning: "he", word_urdu_meaning:"وہ", root_word: "", verb_code: "" , word_count: "481", similar_word: "", isSelect: true , progress_value: 0 ),
      
        Lesson(s_no: 7, lesson_no: "Part - 1b" , word_type : "Noun" , word_name : "هُمْ",word_example: "اُولٰٓئِكَ اَصْحٰبُ الْجَنَّةِ هُمْ فِيْهَا خٰلِدُوْنَ " , eng_translation: "those are the companions of Paradise; they will abide therein eternally", urdu_translation: "وہی ہیں جنت کے رہنے والے وہ اسی میں ہمیشہ رہیں گے ", word_eng_meaning: "they", word_urdu_meaning:"وہ سب", root_word: "", verb_code: "" , word_count: "444", similar_word: "", isSelect: false , progress_value: 0),
      
        Lesson(s_no: 8, lesson_no: "Part - 1b" , word_type : "Noun" , word_name : "أَنْتَ", word_example: "لَّاۤ اِلهَ اِلَّاۤ اَنْتَ سُبْحٰنَكَ اِنِّىْ كُنْتُ مِنَ الظّٰلِمِيْنَ" , eng_translation: "There is no deity except You; exalted are You. Indeed, I have been of the wrongdoers", urdu_translation: "آپ کے سوا کوئی معبود نہیں ،آپ (سب نقائص) سے پاک ہیں ،میں بے شک قصور وار ہوں",  word_eng_meaning: "you", word_urdu_meaning:"آپ", root_word: "", verb_code: "" , word_count: "81", similar_word: "" , isSelect: true , progress_value: 0),
        
        Lesson(s_no: 9, lesson_no: "Part - 1b" , word_type : "Noun" ,word_name : "أَنَا", word_example: "وَّمَاۤ اَنَا مِنَ الْمُشْرِكِيْنَ " , eng_translation: "and I am not of those who associate others with Him", urdu_translation: " اور میں شرک کرنے والوں میں سے نہیں ہوں ", word_eng_meaning: "I",word_urdu_meaning: "میں", root_word: "", verb_code: "" , word_count: "68", similar_word: "", isSelect: false , progress_value: 0)
        
   ]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lesson_name_tableview!.dataSource = self
        self.lesson_name_tableview!.delegate = self
        self.search_tf.delegate = self
        // Do any additional setup after loading the view.
        settingItem = Lessons_ViewController.items
        searchBarArray = Lessons_ViewController.items
        
        //this code is used for delete all the values from userdefault
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
//        UserDefaults.standard.synchronize()
//
      searchController.dimsBackgroundDuringPresentation = false
    }
  
//    private func searchBarSetup(){
//        searchController.searchResultsUpdater = self
//        searchController.searchBar.delegate = self
//        navigationItem.searchController = searchController
//
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive{
            return  searchBarArray.count
        }else{
            return Lessons_ViewController.items.count
    }
}
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
//        var returnCell = UITableViewCell()
        //if searchController.isActive{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Lesson_TableViewCell", for: indexPath) as! Lesson_TableViewCell
       if searchController.isActive{
                   cell.lesson_no_lbl.text = searchBarArray[indexPath.row].lesson_no
                   cell.word_name_lbl.text = searchBarArray[indexPath.row].word_name
                   cell.word_type_lbl.text = searchBarArray[indexPath.row].word_type
                   
                   if searchBarArray[indexPath.row].isSelect == true {
                       cell.completed_lbl.text = "Completed"
                   }else if searchBarArray[indexPath.row].isSelect == false{
                       cell.completed_lbl.text = "Pending"
                       cell.completed_lbl.textColor = UIColor.red
                   }else {
                       cell.completed_lbl.text = ""
                   }
                   if cell.word_type_lbl.text == "Noun"{
                       cell.word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
                   }else if cell.word_type_lbl.text == "Verb"{
                       cell.word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
                   }else{
                       cell.word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
                   }
       }else{
            cell.lesson_no_lbl.text = Lessons_ViewController.items[indexPath.row].lesson_no
            cell.word_name_lbl.text = Lessons_ViewController.items[indexPath.row].word_name
            cell.word_type_lbl.text = Lessons_ViewController.items[indexPath.row].word_type
            
            if Lessons_ViewController.items[indexPath.row].isSelect == true {
                cell.completed_lbl.text = "Completed"
            }else if Lessons_ViewController.items[indexPath.row].isSelect == false{
                cell.completed_lbl.text = "Pending"
                cell.completed_lbl.textColor = UIColor.red
            }else {
                cell.completed_lbl.text = ""
            }
            if cell.word_type_lbl.text == "Noun"{
                cell.word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
            }else if cell.word_type_lbl.text == "Verb"{
                cell.word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            }else{
                cell.word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if tableView == lesson_name_tableview {
         if searchController.isActive{
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            controller.selectedLesson = self.searchBarArray
            controller.indexSelect = indexPath
            controller.selectedItem = self.searchBarArray[indexPath.row]
            searchController.isActive = false
            self.navigationController?.pushViewController(controller, animated: true)
         }else{
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        controller.selectedLesson = self.settingItem
        controller.indexSelect = indexPath
        controller.selectedItem = self.settingItem[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
        }
//        }else{
//
//        }
    }

    @IBAction func setMenu_btn_action(_ sender: Any) {
        
         let menu = storyboard!.instantiateViewController(withIdentifier: "Root_ViewController") as! Root_ViewController
        present(menu, animated: true, completion: nil)
    }
    
    @IBAction func search_btn_action(_ sender: Any) {
       
        searchController.hidesNavigationBarDuringPresentation = false
        present(searchController, animated: true, completion: nil)
        self.searchController.searchBar.delegate = self
              
        searchController.searchResultsUpdater = self
        dataModelset()
        
        
//        if isChecked {
//        search_tf.isHidden = false
//        }else{
//             search_tf.isHidden = true }
//        isChecked = !isChecked
    }
    //MARK:- UITableView Delegate
      /*func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          search_tf.resignFirstResponder()
          self.searchBarArray.removeAll()
          searchController.searchResultsUpdater = self
          return true
      }
      func textFieldShouldClear(_ textField: UITextField) -> Bool {
          if search_tf.text?.count != 0 {
              self.searchBarArray.removeAll()
             searchController.searchResultsUpdater = self
          }
         // lesson_name_tableview?.reloadData()
          return true
      }*/
    func dataModelset(){
        searchBarArray = [
            Lesson(s_no: 1 , lesson_no: "Part - 1a" , word_type : "Verb" ,word_name : "اَعُوْذُ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "I seek refuge",word_urdu_meaning: "", root_word: "ع و ذ", verb_code: "(قا)", word_count: "7", similar_word: "" , isSelect: true , progress_value: 0),
            
            Lesson(s_no: 2, lesson_no: "Part - 1a" , word_type : "Noun" ,word_name : "اللّه", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "Allah",word_urdu_meaning: "اللّه", root_word: "", verb_code: "" , word_count: "2550", similar_word: "", isSelect: true , progress_value: 0),
            
            Lesson(s_no: 3, lesson_no: "Part - 1a" , word_type : "Letter" ,word_name : "مِنْ", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "from", word_urdu_meaning: "سے", root_word: "", verb_code: "" , word_count: "2471", similar_word: "" , isSelect: false , progress_value: 0),
            
            Lesson(s_no: 4, lesson_no: "Part - 1a" , word_type : "Noun" , word_name : "شَيْطَان" , word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "satan", word_urdu_meaning: "شیطان", root_word: "ش ط ن", verb_code: "" , word_count: "88", similar_word: "شَيَاطِيْن", isSelect: true , progress_value: 0),
            
            Lesson(s_no: 5, lesson_no: "Part - 1a" , word_type : "Noun" , word_name : "رَجِيْم", word_example: "اَعُوْذُ بِاللهِ مِنَ الشَّيْطٰنِ الرَّجِيْمِ" , eng_translation: "I seek refuge in Allah from Satan, the outcast", urdu_translation: "میں پناہ میں آتا ہوں اﷲ کی شیطان سے جو مردود ہے", word_eng_meaning: "the outcast", word_urdu_meaning: "مردود، دھتکارا ہوا", root_word: "ر ج م", verb_code: "(نـ)" , word_count: "6", similar_word: "", isSelect: false , progress_value: 0),
            
            Lesson(s_no: 6, lesson_no: "Part - 1b" , word_type : "Noun" , word_name : "هُوَ", word_example: "قُلْ هُوَ اللهُ اَحَدٌ " , eng_translation: "Say, “He is Allah, (Who is) One", urdu_translation: "کہہ دیجیے! وہ اللہ ایک ہے", word_eng_meaning: "he", word_urdu_meaning:"وہ", root_word: "", verb_code: "" , word_count: "481", similar_word: "", isSelect: true , progress_value: 0 ),
            
            Lesson(s_no: 7, lesson_no: "Part - 2b" , word_type : "Noun" , word_name : "هُمْ",word_example: "اُولٰٓئِكَ اَصْحٰبُ الْجَنَّةِ هُمْ فِيْهَا خٰلِدُوْنَ " , eng_translation: "those are the companions of Paradise; they will abide therein eternally", urdu_translation: "وہی ہیں جنت کے رہنے والے وہ اسی میں ہمیشہ رہیں گے ", word_eng_meaning: "they", word_urdu_meaning:"وہ سب", root_word: "", verb_code: "" , word_count: "444", similar_word: "", isSelect: false , progress_value: 0),
            
            Lesson(s_no: 8, lesson_no: "Part - 2b" , word_type : "Noun" , word_name : "أَنْتَ", word_example: "لَّاۤ اِلهَ اِلَّاۤ اَنْتَ سُبْحٰنَكَ اِنِّىْ كُنْتُ مِنَ الظّٰلِمِيْنَ" , eng_translation: "There is no deity except You; exalted are You. Indeed, I have been of the wrongdoers", urdu_translation: "آپ کے سوا کوئی معبود نہیں ،آپ (سب نقائص) سے پاک ہیں ،میں بے شک قصور وار ہوں",  word_eng_meaning: "you", word_urdu_meaning:"آپ", root_word: "", verb_code: "" , word_count: "81", similar_word: "" , isSelect: true , progress_value: 0),
            
            Lesson(s_no: 9, lesson_no: "Part - 2b" , word_type : "Noun" ,word_name : "أَنَا", word_example: "وَّمَاۤ اَنَا مِنَ الْمُشْرِكِيْنَ " , eng_translation: "and I am not of those who associate others with Him", urdu_translation: " اور میں شرک کرنے والوں میں سے نہیں ہوں ", word_eng_meaning: "I",word_urdu_meaning: "میں", root_word: "", verb_code: "" , word_count: "68", similar_word: "", isSelect: false , progress_value: 0)
            
        ]
    }
}
extension Lessons_ViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else { return }
        if searchText == ""{
            dataModelset()
       }else{
            searchBarArray = searchBarArray.filter{
                $0.lesson_no.contains(searchText)
           }
        }
        lesson_name_tableview!.reloadData()
    }
}
