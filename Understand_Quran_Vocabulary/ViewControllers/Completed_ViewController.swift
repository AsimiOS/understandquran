//
//  Completed_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/5/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class Completed_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    var completed_array = Constant.completed_word_array
    
    @IBOutlet weak var list_count_lbl: UILabel!
    
    @IBOutlet weak var completes_tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        completes_tableView.delegate = self
        completes_tableView.dataSource = self
      
        list_count_lbl.text = "Completed Words (\(completed_array.count))"
        completes_tableView.reloadData()
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completed_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Completed_TableViewCell", for: indexPath) as? Completed_TableViewCell
       
        
        cell?.lesson_no_lbl.text = completed_array[indexPath.row].lesson_no

        cell?.word_name_lbl.text = completed_array[indexPath.row].word_name
        cell?.word_type.text = completed_array[indexPath.row].word_type
       
        if cell?.word_type.text == "Noun"{
            cell?.word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
        }else if cell?.word_type.text == "Verb"{
            cell?.word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            
        }else{
            cell?.word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let controller = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
            controller.selectedItem = completed_array[indexPath.row]
            controller.selectedLesson = completed_array
            controller.indexSelect = indexPath
            self.navigationController?.pushViewController(controller, animated: true)
        }
    
    
}
