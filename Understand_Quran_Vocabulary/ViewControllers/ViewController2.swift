//
//  ViewController2.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/3/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    
    @IBOutlet weak var s_no_lbl: UILabel!
    @IBOutlet weak var lesson_no_lbl: UILabel!
    @IBOutlet weak var noun_imageView: UIImageView!
    @IBOutlet weak var verb_imageView: UIImageView!
    @IBOutlet weak var letter_imageView: UIImageView!
    
    @IBOutlet weak var word_count: UILabel!
    @IBOutlet weak var word_name_lbl: UILabel!
    @IBOutlet weak var root_words_lbl: UILabel!
    @IBOutlet weak var Verb_code_lbl: UILabel!
    @IBOutlet weak var English_wordMeaning: UILabel!
    @IBOutlet weak var urdu_wordMeaning: UILabel!
    @IBOutlet weak var similar_words_lbl: UILabel!
    
    @IBOutlet weak var word_example: UILabel!
    @IBOutlet weak var english_Translation_lbl: UILabel!
    @IBOutlet weak var urdu_translation: UILabel!
    
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var yes_image: UIImageView!
    @IBOutlet weak var no_image: UIImageView!
    @IBOutlet weak var progress_bar_value: UILabel!
   
    @IBOutlet weak var previous_btn: UIButton!
    @IBOutlet weak var next_btn: UIButton!
    @IBOutlet weak var next_lbl: UILabel!
    @IBOutlet weak var previous_lable: UILabel!
    
    
    var current: Int = 0
    let maxValue = 10
    var progressBarValue : Float = 0
    
    var selectedItem: Lesson?
    var indexPath_value: IndexPath?
    var selectedLesson : [Lesson]?
    
    var indexSelect : IndexPath?
    var checkCount : Int?
    var isCheck: Bool?
    var isRadio_select: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        isCheck = false
//        isRadio_select = false
        // Do any additional setup after loading the view.
        english_Translation_lbl.layer.borderWidth = 0.5
        
        english_Translation_lbl.layer.cornerRadius = 1
        english_Translation_lbl.layer.borderColor = UIColor.lightGray.cgColor
        
        urdu_translation.layer.borderWidth = 0.5
        urdu_translation.layer.cornerRadius = 1
        urdu_translation.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        checkCount = indexSelect?.row
        
        let obj : Lesson = (selectedLesson![(indexSelect?.row)!])
        
        asignValue(object: obj)
        
        progressView.progress = 0.0
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 10)
        progressView.layer.cornerRadius = 5
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 7
        progressView.subviews[1].clipsToBounds = true
    }
   
    func asignValue(object : Lesson ){
        
        s_no_lbl.text = String(object.s_no)
        lesson_no_lbl.text = object.lesson_no
        word_name_lbl.text = object.word_name
        root_words_lbl.text = object.root_word
        Verb_code_lbl.text = object.verb_code
        English_wordMeaning.text = object.word_eng_meaning
        urdu_wordMeaning.text = object.word_urdu_meaning
        similar_words_lbl.text = object.similar_word
        word_example.text = object.word_example
        english_Translation_lbl.text = object.eng_translation
        urdu_translation.text = object.urdu_translation
        word_count.text = object.word_count
        progress_bar_value.text = "\(object.progress_value)"
        
        if object.word_type == "Noun"{
            verb_imageView.image = UIImage(named: "Asset 27")
            letter_imageView.image = UIImage(named: "Asset 28")
            
            noun_imageView.image = UIImage(named: "Asset 30")
            noun_imageView.backgroundColor =  UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
            word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
        }else if object.word_type == "Verb"{
            noun_imageView.image = UIImage(named: "Asset 29")
            letter_imageView.image = UIImage(named: "Asset 28")
            
            verb_imageView.image = UIImage(named: "Asset 30")
            verb_imageView.backgroundColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            
        }else{
            noun_imageView.image = UIImage(named: "Asset 29")
            verb_imageView.image = UIImage(named: "Asset 27")
            
            letter_imageView.image = UIImage(named: "Asset 30")
            letter_imageView.backgroundColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
            word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
        }
        
        if object.isSelect == true {
            yes_image.image = UIImage(named: "Asset 43")
            no_image.image = UIImage(named: "Asset 35")
              isCheck = true
        }else if object.isSelect == false {
            no_image.image = UIImage(named: "Asset 44")
            yes_image.image = UIImage(named: "Asset 37")
            isCheck = false
        }else{
            yes_image.image = UIImage(named: "Asset 37")
            no_image.image = UIImage(named: "Asset 35")
        }
    }
    
    func incrementCount(number: inout Int) {
        number += 1
    }
    func decrementCount(number: inout Int) {
        number -= 1
    }
    
    @IBAction func no_btn_action(_ sender: Any) {
        
        if isCheck == true {
            no_image.image = UIImage(named: "Asset 44")
            yes_image.image = UIImage(named: "Asset 37")
            
            current-=1
            
            decrementCount(number: &current)
            current += 1
            progressBar()
            isCheck = false
            isRadio_select = true
            
            var obj : Lesson = (selectedLesson![checkCount!])
            obj.isSelect = false
            Constant.pending_word_array.append(obj)
            
        }else{
            print("already check no btn")
            no_image.image = UIImage(named: "Asset 44")
            yes_image.image = UIImage(named: "Asset 37")
            isRadio_select = true
        }
        
    }
    
    @IBAction func yes_btn_action(_ sender: Any) {
        if isCheck == false {
            yes_image.image = UIImage(named: "Asset 43")
            no_image.image = UIImage(named: "Asset 35")
            
            current+=1
            progressBar()
            isCheck = true
            isRadio_select = true
            
            var obj : Lesson = (selectedLesson![checkCount!])
            obj.isSelect = true
            obj.progress_value = Int(progressBarValue)
            Constant.completed_word_array.append(obj)
            
        }else{
            print("already checked")
        }
    }
    @IBAction func next_btn(_ sender: Any) {
        print(checkCount!)
        if isRadio_select == false {
            let alert = UIAlertController.init(title: "Attention!", message: "Do you Understand word, Please select one option(Y/N)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            
        previous_btn.isHidden = false
        previous_lable.isHidden = false
        incrementCount(number: &checkCount!)
        
        if checkCount == selectedLesson?.count{
            next_btn.isHidden = true
            next_lbl.isHidden = true
        }else{
        let obj : Lesson = (selectedLesson![checkCount!])
        asignValue(object: obj)
        next_btn.isHidden = false
        next_lbl.isHidden = false
//            isCheck = false
        }
      }
    }
    @IBAction func previous_btn(_ sender: Any) {
        if isRadio_select == false {
            let alert = UIAlertController.init(title: "Attention!", message: "Do you Understand word, Please select one option(Y/N)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            print("this is previous btn click when the yes no selected......")
        }
        if checkCount! > 0 {
            decrementCount(number: &checkCount!)
            let obj : Lesson = (selectedLesson![checkCount!] )
            asignValue(object: obj)
            
            next_btn.isHidden = false
            next_lbl.isHidden = false
        }else{
            previous_btn.isHidden = true
            previous_lable.isHidden = true
        }
    }

    
    func progressBar() {
        let i = current
        var saveProgressValue: Float
        
        if i <= maxValue {
            let ratio = Float(i) / Float(maxValue)
            
            progressView.progress = Float(ratio)
            saveProgressValue = progressView.progress
            progressBarValue = saveProgressValue
            print(progressBarValue)
            progress_bar_value.text = "\(progressBarValue)"
            
            
        }
    }
    
//    func unique(lessons: [Lesson]) -> [Lesson] {
//
//                var uniqueLesson = [Lesson]()
//
//        for car in lessons {
//            if !uniqueLesson.contains(car) {
//                uniqueLesson.append(car)
//            }
//        }
//        return uniqueLesson
//
//    }

    
/*extension Lesson: Equatable {
        
        mutating func removeDuplicate() {
            var testArray: [Element] = []
            
            for i in self {
                if !testArray.contains(i){
                    testArray.append(i)
                }
            }
            self = testArray
            
        }
}*/

}
