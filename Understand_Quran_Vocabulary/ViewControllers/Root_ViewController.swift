//
//  Root_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/3/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class Root_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func homeScreen_action(_ sender: Any) {
   let vc = storyboard?.instantiateViewController(withIdentifier: "Lessons_ViewController") as? Lessons_ViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func completed_word_action(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Completed_ViewController") as? Completed_ViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func pending_word_action(_ sender: Any) {
    
        let vc = storyboard?.instantiateViewController(withIdentifier: "PendingWords_ViewController") as? PendingWords_ViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func see_result_btn(_ sender: Any) {
   
        let vc = storyboard?.instantiateViewController(withIdentifier: "Result_ViewController") as? Result_ViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func contact_btn_action(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Contact_ViewController") as? Contact_ViewController
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func facebook_btn(_ sender: Any) {
        if let url = URL(string: "https://www.facebook.com/understandquran/"){
            UIApplication.shared.open(url)
        }
    }

    @IBAction func youtube_btn(_ sender: Any) {
        if let url = URL(string: "https://www.youtube.com/user/understandquran/videos"){
            UIApplication.shared.open(url)
        }
    }
}
