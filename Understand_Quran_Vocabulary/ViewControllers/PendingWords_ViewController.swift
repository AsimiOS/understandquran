//
//  PendingWords_ViewController.swift
//  Understand_Quran_Vocabulary
//
//  Created by MacBook on 8/7/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class PendingWords_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    let pending_word = Constant.pending_word_array
    
    @IBOutlet weak var list_count_lbl: UILabel!
    
    @IBOutlet weak var pending_tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pending_tableView.delegate = self
        pending_tableView.dataSource = self
        
        list_count_lbl.text = "Pending Words (\(pending_word.count))"
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pending_word.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pending_TableViewCell", for: indexPath) as? pending_TableViewCell
        cell?.lesson_no_lbl.text = pending_word[indexPath.row].lesson_no
        cell?.word_name_lbl.text = pending_word[indexPath.row].word_name
        
        cell?.word_type.text = pending_word[indexPath.row].word_type
        
        if cell?.word_type.text == "Noun"{
            cell?.word_name_lbl.textColor = UIColor(red:0.01, green:0.22, blue:1.00, alpha:1.0)
        }else if cell?.word_type.text == "Verb"{
            cell?.word_name_lbl.textColor = UIColor(red:0.76, green:0.12, blue:0.22, alpha:1.0)
            
        }else{
            cell?.word_name_lbl.textColor = UIColor(red:1.00, green:0.45, blue:0.00, alpha:1.0)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        controller.selectedItem = pending_word[indexPath.row]
        controller.selectedLesson = pending_word
        controller.indexSelect = indexPath
        self.navigationController?.pushViewController(controller, animated: true)
    }}
